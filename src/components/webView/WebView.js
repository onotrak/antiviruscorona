import React from 'react';
import {
	Platform,
} from 'react-native';
import WebView from 'react-native-webview';
import styles from './WebViewStyle';
import Colors from '../../styles/colors';

const onIOS = Platform.OS === 'ios';

const ComponentWebView = ({uri}) => {
	return <WebView source={{ uri: uri }} />
};

export default ComponentWebView;