import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  BackHandler,
  TouchableOpacity,
  Alert,
  Dimensions,
  ActivityIndicator,
  ImageBackground,
} from 'react-native';
import {connect} from 'react-redux';
import { Actions } from 'react-native-router-flux';
import Loader from '../../components/loader/Loader';
import { normalize, showMessage } from '../../components/helpers/helpers';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import MapView, { PROVIDER_GOOGLE } from 'react-native-maps';
import Colors from '../../styles/colors';
import Shadow from '../../styles/shadow';
import GetLocation from 'react-native-get-location'
import Carousel from 'react-native-snap-carousel';
import { mainAction } from '../../redux/actions';
import { RESET_ARTICLE } from '../../redux/types';
import data from '../../../dummy2.json';

const { height, width } = Dimensions.get( 'window' );
const LATITUDE_DELTA = 0.27;
// const LONGITUDE_DELTA = LATITUDE_DELTA * (width / height)
const LONGITUDE_DELTA = 0.123;


class Maps extends React.Component {
   constructor() {
      super();
      this.state = {
         isLoading: false,
         size: 0.275,
         traffic: false,
         mapType: 'standard',

         mapRegion: { 
            // latitude: -1.552372, //indo
            // longitude: 120.136036, //indo
            latitude: -7.797068,
            longitude: 110.370529,
            latitudeDelta: LATITUDE_DELTA, 
            longitudeDelta: LONGITUDE_DELTA 
         },
         
         page: 1,
         refreshing: false,
         data: [],
         activeSlide: 0,
         activeDesc: null,
         activeTitle: null,
         activeImage: null,
         showDetail: false,
      };
   }
   
   componentDidMount() {
      // this.fetchRecords(this.state.page)
      this.setState({
         data: data.dataProvince,
         activeImage: data.dataProvince[0].image,
         activeDesc: data.dataProvince[0].description,
         activeTitle: data.dataProvince[0].name
      })
      BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
   }

   componentWillReceiveProps(nextProps) {
      console.log('WRP', this.props.main)
      const {articleData, articleSuccess, articleError} = this.props.main
      // const {data} = this.state
      setTimeout(() => {
         
      }, 2000)
      this.setState({isLoading: false, refreshing: false})
      
      // if(articleSuccess){
      //    this.setState({
      //       data: data.concat(articleData),
      //       activeImage: data.concat(articleData)[0].image,
      //       activeDesc: data.concat(articleData)[0].description,
      //       activeTitle: data.concat(articleData)[0].title
      //    })
      //    console.log('onotrak data', data.concat(articleData))
      // }else(
      //    showMessage('Get Article Error', 'long')
      // )
   }
   
   componentDidUpdate(prevProps, prevState) {
      const {articleSuccess, articleError } = this.props.main;
      if (articleSuccess, articleError) {
         setTimeout(() => {
            this.props.dispatch({type: RESET_ARTICLE});
         }, 500);
      }
   }

   componentWillUnmount() {
      BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
   }
   
   render(){
      const {data, refreshing, activeSlide, showDetail} = this.state
      console.log('onotrak data', data)
      return (
         <View styl={styles.container}>
            <Loader show={this.state.isLoading} />
            <View style={styles.content}>
               <MapView
                  style={{flex: 1}}
                  region={this.state.mapRegion}
                  mapType={this.state.mapType}
                  showsUserLocation={true}
                  followUserLocation={true}
                  // showsTraffic={this.state.traffic}

                  // provider={PROVIDER_GOOGLE}
                  // initialRegion={{
                  //    // latitude: 48.882004,
                  //    // longitude: 74.582748,
                  //    // latitudeDelta: 0.0922,
                  //    // longitudeDelta: 0.0421,
                  //    latitude: this.state.latitude,
                  //    longitude: this.state.longitude,
                  //    latitudeDelta: this.state.latitudeDelta,
                  //    longitudeDelta: this.state.longitudeDelta,
                  // }}
                  // showsUserLocation

                  //RN
                  // style={{flex: 1}}
                  // showsUserLocation = {false}
                  // followUserLocation = {false}
                  // zoomEnabled = {true}
               />
               <View style={{ width: 30,position:'absolute', alignItems:'center', zIndex:11001, bottom: 20, right:20 }}>
                  <TouchableOpacity style={[styles.iconStyle, Shadow.shadow, {marginVertical:10}]} onPress={()=> this.getLocation()}>
                     <Icon name='map-marker-outline' size={30} color={Colors.white} />
                  </TouchableOpacity> 
                  <TouchableOpacity style={[styles.iconStyle, Shadow.shadow, {marginVertical:10}]} 
                     onPress={()=> 
                        this.setState({
                           mapRegion: { 
                              latitude: -7.797068,
                              longitude: 110.370529,
                              latitudeDelta: LATITUDE_DELTA, 
                              longitudeDelta: LONGITUDE_DELTA 
                           },
                           size: 0.275,
                        })
                     }>
                     <Icon name='reload' size={30} color={Colors.white} />
                  </TouchableOpacity> 
                  <TouchableOpacity style={[styles.iconStyle, Shadow.shadow, {marginTop:10}]} onPress={()=> this.plus()}>
                     <Icon name='plus-circle-outline' size={30} color={Colors.white} />
                  </TouchableOpacity> 
                  <TouchableOpacity style={[styles.iconStyle, Shadow.shadow, {marginTop:10}]} onPress={()=> this.minus()}>
                     <Icon name='minus-circle-outline' size={30} color={Colors.white} />
                  </TouchableOpacity> 
               </View>
               <View style={{ width: 30,position:'absolute', alignItems:'center', zIndex:11001, bottom: 20, left:20 }}>
                  <TouchableOpacity style={[styles.iconStyle, Shadow.shadow, {marginVertical:10, padding: 5, height: 40, width: 40}]} onPress={()=> this.setState({mapType: this.state.mapType === 'standard' ? 'satellite' : 'standard' })}>
                     <Icon name={this.state.mapType === 'standard' ? 'map-outline' : 'satellite-variant'} size={30} color={Colors.white} />
                  </TouchableOpacity>
               </View>
               {
                  showDetail &&
                  <View style={{ width: '100%', height: '30%', position:'absolute', alignItems:'center', zIndex:11001, top: 0, left: 0 }}>
                     <View style={{
                        // flex: 0.5,
                        height: '100%', 
                        flexDirection: 'row', 
                        marginTop: 10,
                        // backgroundColor: Colors.backgroundColor,
                        width: '100%'
                     }}>
                        <Carousel
                           firstItem={activeSlide}
                           data={data}
                           renderItem={this.renderItem}
                           sliderWidth={width}
                           sliderHeight={height}
                           itemWidth={width * (60 / 100)}
                           inactiveSlideScale={1}
                           onSnapToItem={(index, item) => {
                              let activeImage = data[index].image;
                              let activeDesc = data[index].description;
                              let activeTitle = data[index].name;

                              let temp = 2
                              let mapRegion = { 
                                 latitude: data[index].latitude,
                                 longitude: data[index].longitude,
                                 latitudeDelta: temp,
                                 longitudeDelta: temp * (width / height),
                              }

                              this.setState({
                                 activeSlide: index,
                                 activeImage,
                                 activeDesc,
                                 activeTitle,
                                 mapRegion,
                                 size: temp,
                              });
                           }}
                        />
                     </View>
                  </View>
               }
                  
               <View style={{ width: 30, height: '20%', position:'absolute', alignItems:'center', zIndex:11001, top: 30, left: 10, opacity: showDetail ? 0.5 : 0.9 }}>
                  <TouchableOpacity style={[styles.iconStyle, {marginVertical:10, padding: 5, height: '100%', width: 30, borderRadius: 10, justifyContent: 'space-between'}]} onPress={()=> this.setState({showDetail: !showDetail })}>
                     <Icon name={showDetail ? 'arrow-expand-left':'arrow-expand-right'} size={20} color={Colors.white}  />
                     <Icon name={showDetail ? 'arrow-expand-left':'arrow-expand-right'} size={20} color={Colors.white} />
                     <Icon name={showDetail ? 'arrow-expand-left':'arrow-expand-right'} size={20} color={Colors.white} />
                  </TouchableOpacity>
               </View>
            </View>
         </View>
      );
   }

   renderItem = ({item, index}) => {
      return (
         <TouchableOpacity activeOpacity={0.6} style={{marginRight: 20}} 
            onPress={() => Actions.CovidData({dataItem: item.name})}
         >
            <ImageBackground
               defaultSource={require('../../assets/images/antibody_img.jpg')}
               source={{ uri: item.image }}
               style={{
                  alignSelf: "center",
                  width: "100%",
                  height: "100%",
                  overflow: "hidden",
                  borderRadius: 10,
               }}
            >
               <View style={{width: '100%', height: '40%', backgroundColor: Colors.blackTransparent, bottom: 10, position: 'absolute', padding: 10}}>
                  <Text numberOfLines={1} style={{color: Colors.white, fontSize: normalize(15)}}>{item.name}</Text>
                  <Text numberOfLines={2} style={{color: Colors.white, fontSize: normalize(12), marginTop: 5,}}>{item.description}</Text>
               </View>
            </ImageBackground>
        </TouchableOpacity>
      );
   }


   getLocationName = () => {
      const apiKey = 'AIzaSyCLTYXcwj5OfJqS0pZiWI1B184WM2zbNtM'
      fetch('https://maps.googleapis.com/maps/api/geocode/json?address=' + this.state.mapRegion.latitude + ',' + this.state.mapRegion.longitude + '&key=' + apiKey)
         .then((response) => response.json())
         .then((responseJson) => {
            console.log('onotrak responseJson', responseJson);
         }
      )
   }

   getLocation = async () => {
      await this.setState({isLoading: true})
      GetLocation.getCurrentPosition({
         enableHighAccuracy: true,
         timeout: 10000,
      })
      .then(location => {
         let temp = this.state.size < 0.005 ? this.state.size : 0.005
         let mapRegion = { 
            longitude: location.longitude,
            latitude: location.latitude,
            latitudeDelta: temp,
            longitudeDelta: temp * (width / height),
         }
         this.setState({
            mapRegion,
            longitude: location.longitude,
            latitude: location.latitude,
            size: temp,
         })
         console.log('onotrak', mapRegion)
         this.setState({isLoading: false})
      })
      .catch(error => {
         const { code, message } = error;
         showMessage(message, 'long')
         this.setState({isLoading: false})
      })
   }
   
   minus = () => {
      let temp = this.state.size + 0.002
      let mapRegion = { 
         latitude: this.state.mapRegion.latitude,
         longitude: this.state.mapRegion.longitude,
         latitudeDelta: temp,
         longitudeDelta: temp * (width / height)
      }
      this.setState({
         mapRegion,
         size:temp
      })
   }
   plus = () => {
      let temp = this.state.size - 0.002 < 0 ? 0 : this.state.size - 0.002
      let mapRegion = {
         latitude: this.state.mapRegion.latitude,
         longitude: this.state.mapRegion.longitude,
         latitudeDelta: temp,
         longitudeDelta: temp * (width / height),
      }
      this.setState({
         mapRegion,
         size:temp
      })
   }

   fetchRecords = (page) => {
      const {userData} = this.props.main
      this.props.dispatch(mainAction.getArticle(page, userData.token));
   }
   
   onScrollHandler = async () => {
      await this.setState({refreshing: true})
      this.setState({
         page: this.state.page + 1
      }, () => this.fetchRecords(this.state.page));
   }

   handleBackButton = () => {
      Alert.alert(
         "Exit App",
         "Exiting the application?",
         [
            {
               text: "Cancel",
               onPress: () => null,
               style: "cancel"
            },
            {
               text: "OK",
               onPress: () => BackHandler.exitApp()
            }
         ],
         {
            cancelable: false
         }
      );
      return true;
   }

};

const mapStateToProps = ({mainReducer}) => ({
  main: mainReducer,
});

export default connect(mapStateToProps)(Maps);

const styles = StyleSheet.create({
   container: {
      flex: 1,
      backgroundColor: Colors.backgroundColor,
      justifyContent: 'center',
      alignItems: 'center',
   },
   content: {
      width: '100%',
      height: '100%',
   },
   textStyle: {
      fontSize: normalize(20),
      color: 'black',
   },
   textBtn: {
      fontSize: normalize(15)
   },
   iconStyle: {
      borderRadius: 20, 
      // height: 40,
      // width: 40,
      backgroundColor: Colors.primaryGreen1,
   },
   actInd: {
      height: '100%',
      justifyContent: 'center',
      paddingHorizontal: 10,
   },
});
