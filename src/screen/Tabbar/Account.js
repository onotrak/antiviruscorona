import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  BackHandler,
  TouchableOpacity,
  Alert,
  ScrollView,
  Animated,
  Image,
  Easing
} from 'react-native';
import {connect} from 'react-redux';
import { Actions } from 'react-native-router-flux';
import Loader from '../../components/loader/Loader';
import { normalize, showMessage } from '../../components/helpers/helpers';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Icons from 'react-native-vector-icons/Fontisto';
import Colors from '../../styles/colors';
import { LOGOUT } from '../../redux/types';
import GetLocation from 'react-native-get-location';
import provinceData from '../../../dummy.json';

class Account extends React.Component {
   constructor() {
      super();
      this.state = {
         isLoading: false,
         name: '',
         email: '',
         location: false,
         animRun: false,
      };
      this.animatedValue = new Animated.Value(0)
   }

   animate () {
      this.animatedValue.setValue(0)
      const createAnimation = function (value, duration, easing, delay = 0) {
        return Animated.timing(
          value,
          {
            toValue: 1,
            duration,
            easing,
            delay
          }
        )
      }
      Animated.parallel([
        createAnimation(this.animatedValue, 2000, Easing.ease),       
      ]).start(()=> this.state.animRun && this.animate())
    }
   
   componentDidMount() {
      const {isLoggedin, userData} = this.props.main
		if(isLoggedin){
         this.getLocation()
         this.setState({
            name: userData.user.name,
            email: userData.user.email,
         })
      }else{
         Actions.reset('Login')
         showMessage('Please Login to continue!', 'long')
      }
      BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
   }

   componentWillReceiveProps(nextProps) {
   }
   
   componentDidUpdate(prevProps, prevState) {
      
   }

   componentWillUnmount() {
      BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
   }
   
   render(){
      const {name, email} = this.state
      const spinText = this.animatedValue.interpolate({
         inputRange: [0, 1],
         outputRange: ['0deg', '720deg']
      })

      return (
         <View styl={styles.container}>
            <Loader show={this.state.isLoading} />
            <View style={styles.content}>
               <View style={styles.card1}>
                  <Icon name='shield-account' color='black' size={100} />
                  <Text style={styles.name}>{name}</Text>
               </View>
               <ScrollView showsVerticalScrollIndicator= {false}>
                  <Text style={styles.textTitle}>E-mail</Text>
                  <View style={[styles.card2, {width: '100%'}]}>
                     <Text style={styles.textStyle}>{email}</Text>
                  </View>
                  <Text style={styles.textTitle}>Province Location</Text>
                  <View style={[styles.card2, {width: '90%', flexDirection: 'row', justifyContent: 'space-between'}]}>
                     <Text numberOfLines={1} style={[styles.textStyle, {textTransform: 'capitalize', width: '80%', color: !this.state.location ? Colors.red:Colors.primaryGreen4}]}>{this.state.location ? this.state.location : 'Location Error'}</Text>
                     <TouchableOpacity onPress={()=> this.getLocation()} style={{width: 20, height: 20, alignItems: 'center', alignContent: 'center', justifyContent: 'center'}}>
                        <Animated.View style={{transform: [{rotate: spinText}]}}>
                           <Icons name='spinner-refresh' size={20} color={!this.state.location ? Colors.red : Colors.primaryGreen4} />
                        </Animated.View>
                     </TouchableOpacity>
                  </View>
                  <Text style={styles.textTitle}>Status In Radius 5 km</Text>
                  <View style={[styles.card2, {width: '80%', marginBottom: 10}]}>
                     <Text style={styles.textStyle}>0 Orang Dalam Pemantauan</Text>
                  </View>
                  <View style={[styles.card2, {width: '70%', marginBottom: 10}]}>
                     <Text style={styles.textStyle}>0 Pasien Dalam Pengawasan</Text>
                  </View>
                  <View style={[styles.card2, {width: '60%', marginBottom: 10}]}>
                     <Text style={styles.textStyle}>0 Positif Terkena COVID-19</Text>
                  </View>
                  <TouchableOpacity onPress={()=> this.onPressLogout()} style={styles.btnLogin}>
                     <Text style={styles.textBtn}>LOGOUT</Text>
                  </TouchableOpacity>
               </ScrollView>
            </View>
            {/* <View style={styles.viewBtn}>
            </View> */}
         </View>
      );
   }

   getLocation = async () => {
      this.animate()
      await this.setState({animRun: true})
      console.log('onotrak log')
      GetLocation.getCurrentPosition({
         enableHighAccuracy: true,
         timeout: 10000,
      })
      .then(location => {
         let longitude = location.longitude.toString().split('.')
         let latitude = location.latitude.toString().split('.')
         
         let latitudeData = latitude[0]+'.'+latitude[1].substring(0, 1)
         let longitudeData = longitude[0]+'.'+longitude[1].substring(0, 1)

         let name = provinceData.dataProvince.filter(x => {
            return x.latitude.split('.')[0]+'.'+x.latitude.split('.')[1].substring(0, 1) === latitudeData
            && x.longitude.split('.')[0]+'.'+x.longitude.split('.')[1].substring(0, 1) === longitudeData
         })
         if(name.length){
            this.setState({location: name[0].name})
            console.log('onotrak', name)
            showMessage('Province Found!')
         }else{
            showMessage('Error!')
         }
         setTimeout(() => {this.setState({animRun: false})}, 500)
      })
      .catch(error => {
         const { code, message } = error;
         this.setState({location: false})
         setTimeout(() => {this.setState({animRun: false})}, 500)
         showMessage(message, 'long')
      })
   }

   onPressLogout = () => {
      Alert.alert(
         'Log Out',
         'Are you sure? Logging out will remove all account data.',
         [
            {
               text: "Cancel",
               onPress: () => null,
               style: "cancel"
            },
            {
               text: "OK",
               onPress: () => {
                  this.props.dispatch({type: LOGOUT})
                  Actions.reset('Login')
               }
            }
         ],
         {
            cancelable: false
         }
      );
   }

   handleBackButton = () => {
      Alert.alert(
         "Exit App",
         "Exiting the application?",
         [
            {
               text: "Cancel",
               onPress: () => null,
               style: "cancel"
            },
            {
               text: "OK",
               onPress: () => BackHandler.exitApp()
            }
         ],
         {
            cancelable: false
         }
      );
      return true;
   }

};

const mapStateToProps = ({mainReducer}) => ({
  main: mainReducer,
});

export default connect(mapStateToProps)(Account);

const styles = StyleSheet.create({
   container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
   },
   content: {
      height: '100%',
      width: '100%',
      paddingHorizontal: 20,
      backgroundColor: Colors.backgroundColor
   },
   // imgbackgoundStyle: {
   //    width: '100%',
   //    // height: '100%',
   //    padding: 20,
   //    justifyContent: 'center',
   //    alignItems: 'center',
   //    resizeMode: 'stretch',
   // },
   card1: {
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: Colors.primaryGreen1,
      marginBottom: 20,
      borderRadius: 5,
      padding: 20,
   },
   card2: {
      borderRadius: 5,
      padding: 10,
      marginBottom: 15,
      backgroundColor: Colors.backgroundColor,
      // alignItems: 'center',
      justifyContent: 'center',
      borderWidth: 1,
      borderColor: Colors.primaryGreen3
   },
   name: {
      marginTop: 20,
      fontSize: normalize(20),
      fontWeight: 'bold',
      color: Colors.primaryGreen4
   },
   textStyle: {
      fontSize: normalize(13),
      color: Colors.primaryGreen4
   },
   textTitle: {
      fontSize: normalize(11),
      marginBottom: 5,
      color: Colors.primaryGreen4
   },
   viewBtn: {
      position: 'absolute',
      width: '100%',
      bottom: 0,
   },
   textBtn: {
      fontSize: normalize(15),
      color: Colors.white
   },
   btnLogin: {
      alignItems: 'center',
      padding: 10,
      marginHorizontal: 50,
      marginVertical: 20,
      borderRadius: 10,
      backgroundColor: Colors.primaryGreen4
   },
});
