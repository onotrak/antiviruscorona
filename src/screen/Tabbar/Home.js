import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  BackHandler,
  TouchableOpacity,
  Alert,
  Image,
  Dimensions,
} from 'react-native';
import {connect} from 'react-redux';
import { Actions } from 'react-native-router-flux';
import Loader from '../../components/loader/Loader';
import { normalize, showMessage } from '../../components/helpers/helpers';
import Icons from 'react-native-vector-icons/Entypo';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Colors from '../../styles/colors';
import Shadow from '../../styles/shadow';
import Carousel, {Pagination} from 'react-native-snap-carousel';
import { mainAction } from '../../redux/actions';

const sliderWidth = Dimensions.get("window").width;

class Home extends React.Component {
   constructor() {
      super();
      this.state = {
         isLoading: false,
         activeDotIndex: 0,
         articleData: [
            {
               title: "Yellow",
               category: "Category 0",
               image: "http://api.devmind2.net/uploads/img/banner/S2iL2qCeQ2gj8r1tUWi1z6w2yRZpFA5PcB7v8ngq.jpeg",
               description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus cursus orci ut vulputate pharetra. Suspendisse molestie ligula congue dui elementum euismod. Integer luctus risus nec elementum rhoncus.",
               date: "1999-09-26 12:08:51"
            }
         ]
      };
   }
   
   componentDidMount() {
      const {articleData} = this.props.main
      const {userData, isLoggedin} = this.props.main
      if(isLoggedin){
         this.props.dispatch(mainAction.getArticle(1, userData.token));
      }
      if(articleData.length){
         this.setState({
            articleData,
         })
      }

      BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
   }

   componentWillReceiveProps(nextProps) {
      const {articleData, articleSuccess, articleError} = this.props.main
      if(articleSuccess){
         this.setState({
            articleData,
         })
      }
   }
   
   componentDidUpdate(prevProps, prevState) {
      
   }

   componentWillUnmount() {
      BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
   }
   
   render(){
      const {articleData} = this.state
      const {userData, isLoggedin} = this.props.main
      console.log('onotrak home', articleData )
      return (
         <View styl={styles.container}>
            <Loader show={this.state.isLoading} />
            <View style={styles.content}>
               <View style={{flexDirection: 'row', alignSelf: 'flex-start', marginBottom: 10}}>
                  <Text style={styles.textStyle}>Welcome, </Text>
                  <Text style={[styles.textStyle, {fontWeight: 'bold'}]}>
                     {isLoggedin && userData.user.name.toUpperCase()}
                  </Text>
               </View>
               <View style={styles.banerContainer}>
                  <View style={styles.banerContent}>
                     <Carousel
                        data={articleData}
                        sliderWidth={sliderWidth}
                        itemWidth={sliderWidth}
                        onSnapToItem={(index, item)=> this.setState({activeDotIndex: index})}
                        renderItem={({item, index}) => (
                           <TouchableOpacity activeOpacity={0.9} onPress={()=> {
                              if(isLoggedin){
                                 Actions.ArticleDetail({dataItem: item})
                              }else{
                                 Actions.reset('Login')
                                 showMessage('Please Login to continue!', 'long')
                              }
                           }}>
                              <Image
                                 source={{uri: item.image}}
                                 style={styles.imageBanner}
                                 resizeMode='cover'
                              />
                           </TouchableOpacity>
                        )}
                     />
                     <View style={styles.paginationStyle}>
                        <Pagination
                           dotsLength={articleData.length}
                           activeDotIndex={this.state.activeDotIndex}
                           dotStyle={styles.dotStyle}
                           inactiveDotStyle={styles.inactiveDotStyle}
                           inactiveDotOpacity={1}
                           inactiveDotScale={1}
                        />
                     </View>
                  </View>
               </View>
               <View style={{flexDirection: 'row', justifyContent: 'space-between', width: '80%', marginBottom: 10}}>
                  <TouchableOpacity onPress={()=> Actions.Article()} style={[styles.btnView2, Shadow.shadow]}>
                     <Text style={[styles.textBtn, {marginBottom: 5}]}>Article</Text>
                     <Icons name='news' color='black' size={45} />
                  </TouchableOpacity>
                  <TouchableOpacity onPress={()=> Actions.CovidData()} style={[styles.btnView2, Shadow.shadow]}>
                     <Text style={[styles.textBtn, {marginBottom: 0}]}>Covid</Text>
                     <Icon name='table-search' color='black' size={50} />
                  </TouchableOpacity>
               </View>
               <TouchableOpacity onPress={()=> Actions.CovidJogja()} style={[styles.btnView, Shadow.shadow]}>
                  <Text style={styles.textBtn}>Covid Maps DIY</Text>
                  <Icon name='map-search-outline' color='black' size={25} />
               </TouchableOpacity>
            </View>
         </View>
      );
   }

   onPressLogin = () => {
      Actions.Article()
   }

   handleBackButton = () => {
      Alert.alert(
         "Exit App",
         "Exiting the application?",
         [
            {
               text: "Cancel",
               onPress: () => null,
               style: "cancel"
            },
            {
               text: "OK",
               onPress: () => BackHandler.exitApp()
            }
         ],
         {
            cancelable: false
         }
      );
      return true;
   }

};

const mapStateToProps = ({mainReducer}) => ({
  main: mainReducer,
});

export default connect(mapStateToProps)(Home);

const styles = StyleSheet.create({
   container: {
      flex: 1,
      backgroundColor: 'aqua',
      justifyContent: 'center',
      alignItems: 'center',
   },
   content: {
      width: '100%',
      height: '100%',
      backgroundColor: Colors.backgroundColor,
      // justifyContent: 'center',
      alignItems: 'center',
      padding: 20,
   },
   textStyle: {
      fontSize: normalize(15),
      color: Colors.primaryGreen4,
   },
   textBtn: {
      fontSize: normalize(15),
      textAlignVertical: 'center',
      color: Colors.primaryGreen4
   },
   btnLogin: {
      alignItems: 'center',
      padding: 10,
      margin: 15,
      backgroundColor: 'aqua'
   },
   btnView: {
      justifyContent: 'space-between',
      flexDirection: 'row',
      marginBottom: 15,
      paddingHorizontal: 20,
      alignItems: 'center',
      width: '80%',
      height: 50,
      borderWidth: 1,
      backgroundColor: Colors.backgroundColor,
      borderColor: Colors.primaryGreen1,
      borderRadius: 10,
   },
   btnView2: {
      justifyContent: 'center',
      marginBottom: 15,
      alignItems: 'center',
      justifyContent: 'center',
      width: '40%',
      padding: 15,
      borderWidth: 1,
      backgroundColor: Colors.backgroundColor,
      borderColor: Colors.primaryGreen1,
      borderRadius: 10,
   },

   //baner
   banerContainer: {
      marginBottom: 40,
      paddingHorizontal: 23,
   },
   banerContent: {
      height: 135,
      width: '100%',
      borderRadius: 10,
      marginTop: 6,
   },
   imageBanner: {
      borderRadius: 10,
      width: '90%',
      height: '100%',
      marginHorizontal: 20,
   },
   paginationStyle: {
      position: 'absolute', 
      bottom: -10, 
      width: '100%',
      alignSelf: 'center',
   },
   dotStyle: {
      width: 50,
      height: 5,
      borderRadius: 2.5,
      marginHorizontal: 0,
      backgroundColor: Colors.white
   },
   inactiveDotStyle: {
      width: 5,
      height: 5,
      borderRadius: 2.5,
      marginHorizontal: 0,
      backgroundColor: Colors.white
   }
});
