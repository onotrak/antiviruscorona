import React from 'react';
import {
  BackHandler,
} from 'react-native';
import {connect} from 'react-redux';
import { Actions } from 'react-native-router-flux';
import WebView from 'react-native-webview';

class CovidJogja extends React.Component {
   constructor() {
      super();
      this.state = {
         isLoading: false,
      };
   }
   
   componentDidMount() {
      BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
   }

   componentWillReceiveProps(nextProps) {
   }
   
   componentDidUpdate(prevProps, prevState) {
      
   }

   componentWillUnmount() {
      BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
   }
   
   render(){
      return <WebView source={{ uri: 'https://public.tableau.com/shared/KFT4KN5PF?:toolbar=n&:display_count=y&:origin=viz_share_link&:embed=y&:showVizHome=no' }} />
   }

   handleBackButton = () => {
      Actions.pop()
      return true;
   }

};

const mapStateToProps = ({mainReducer}) => ({
  main: mainReducer,
});

export default connect(mapStateToProps)(CovidJogja);
