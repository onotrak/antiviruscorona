import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  BackHandler,
  TouchableOpacity,
  Alert,
  Image,
  ScrollView,
  Dimensions,
} from 'react-native';
import {connect} from 'react-redux';
import { Actions } from 'react-native-router-flux';
import Loader from '../components/loader/Loader';
import { normalize } from '../components/helpers/helpers';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Colors from '../styles/colors';
import Shadow from '../styles/shadow';

const width = Dimensions.get("window").width;
const height = Dimensions.get("window").height;

class ArticleDetail extends React.Component {
   constructor() {
      super();
      this.state = {
         isLoading: false,
      };
   }
   
   componentDidMount() {
      BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
   }

   componentWillReceiveProps(nextProps) {
   }
   
   componentDidUpdate(prevProps, prevState) {
      
   }

   componentWillUnmount() {
      BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
   }
   
   render(){
      const {dataItem} = this.props
      return (
         <View styl={styles.container}>
            <Loader show={this.state.isLoading} />
            <View style={styles.header}>
               <TouchableOpacity onPress={()=> this.handleBackButton()} style={styles.btnHeader}>
                  <Icon name='chevron-left' color={Colors.white} size={30} />
               </TouchableOpacity>
               <Text style={styles.textHeader}>Article {dataItem.title}</Text>
            </View>
            <ScrollView>
            <View style={styles.content}>
               <Image source={{uri: dataItem.image}} style={{width: '100%', height: 200}} resizeMode='cover' />
               <View style={[styles.descView, Shadow.shadow]}>
                  <Text style={{color: Colors.primaryGreen4, fontSize: normalize(20), textAlign: 'center', marginBottom: 15 }}>{dataItem.title}</Text>
                  <Text style={{color: Colors.primaryGreen4, fontSize: normalize(15), marginBottom: 10 }}>{dataItem.category}</Text>
                  <Text style={{color: Colors.primaryGreen4, fontSize: normalize(15), marginBottom: 10 }}>Date: {dataItem.date}</Text>
                  <Text style={{color: Colors.primaryGreen4, fontSize: normalize(15), }}>{dataItem.description}</Text>
               </View>
               </View>
            </ScrollView>
         </View>
      );
   }

   onPressLogin = () => {
      this.setState({isLoading: true})
      setTimeout(() => {
         this.setState({isLoading: false})
         Actions.Login()
      }, 2000)
   }

   handleBackButton = () => {
      Actions.pop()
      return true;
   }

};

const mapStateToProps = ({mainReducer}) => ({
  main: mainReducer,
});

export default connect(mapStateToProps)(ArticleDetail);

const styles = StyleSheet.create({
   container: {
      // flex: 1
   },
   content: {
      // flex: 0.92,
      height: height-70,
      // justifyContent: 'center',
      // alignItems: 'center',
      backgroundColor: Colors.backgroundColor
   },
   header: {
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: Colors.primaryGreen3,
      width: '100%',
      height: 50,
   },
   textHeader: {
      width: '85%',
      color: Colors.white,
      textAlignVertical: 'center',
      fontSize: normalize(17),
      paddingVertical: 15,
      // textTransform: ''
   },
   btnHeader: {
      width: '15%',
      paddingHorizontal: 15,
   },
   textStyle: {
      fontSize: normalize(20),
      color: 'black',
   },
   textBtn: {
      fontSize: normalize(15)
   },
   btnLogin: {
      alignItems: 'center',
      padding: 10,
      margin: 15,
      backgroundColor: 'aqua'
   },
   descView: {
      flex: 0.95, 
      borderRadius: 10, 
      padding: 20, 
      borderWidth: 1, 
      backgroundColor: Colors.backgroundColor,
      borderColor: Colors.primaryGreen1, 
      margin: 20
   },
});
