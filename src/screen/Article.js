import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  BackHandler,
  TouchableOpacity,
  Dimensions,
  ActivityIndicator,
  ImageBackground,
} from 'react-native';
import {connect} from 'react-redux';
import { Actions } from 'react-native-router-flux';
import Loader from '../components/loader/Loader';
import { normalize, showMessage } from '../components/helpers/helpers';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { mainAction } from '../redux/actions';
import { RESET_ARTICLE, RESET_DATA_ARTICLE } from '../redux/types';
import Colors from '../styles/colors';
import Shadow from '../styles/shadow';
import Carousel from 'react-native-snap-carousel';

const width = Dimensions.get("window").width;
const height = Dimensions.get("window").height;

class Article extends React.Component {
   constructor() {
      super();
      this.state = {
         isLoading: true,
         page: 1,
         refreshing: false,
         data: [],
         
         activeSlide: 0,
         activeDesc: null,
         activeTitle: null,
         activeImage: null
      };
   }
   
   componentDidMount() {
      const {isLoggedin} = this.props.main
		if(isLoggedin){
         this.fetchRecords(this.state.page)
      }else{
         Actions.reset('Login')
         showMessage('Please Login to continue!', 'long')
      }

      BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
   }

   componentWillReceiveProps(nextProps) {
      console.log('WRP', this.props.main)
      const {articleData, articleSuccess, articleError} = this.props.main
      const {data} = this.state
      setTimeout(() => {
         
      }, 2000)
      this.setState({isLoading: false, refreshing: false})
      
      if(articleSuccess){
         this.setState({
            data: data.concat(articleData),
            activeImage: data.concat(articleData)[0].image,
            activeDesc: data.concat(articleData)[0].description,
            activeTitle: data.concat(articleData)[0].title
         })
         console.log('onotrak data', data.concat(articleData))
      }else(
         showMessage('Get Article Error', 'long')
      )
   }
   
   componentDidUpdate(prevProps, prevState) {
      const {articleSuccess, articleError } = this.props.main;
      if (articleSuccess, articleError) {
         setTimeout(() => {
            this.props.dispatch({type: RESET_ARTICLE});
         }, 500);
      }
   }

   componentWillUnmount() {
      BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
   }
   
   render(){
      const {data, refreshing, activeDesc, activeSlide, activeImage, activeTitle} = this.state
      return (
         <View styl={styles.container}>
            <Loader show={this.state.isLoading} />
            <View style={styles.header}>
               <TouchableOpacity onPress={()=> this.handleBackButton()} style={styles.btnHeader}>
                  <Icon name='chevron-left' color={Colors.white} size={30} />
               </TouchableOpacity>
               <Text style={styles.textHeader}>Article Menu</Text>
            </View>
            <View style={styles.content}>
               <View style={{flex: 0.5, flexDirection: 'row'}}>
                  <Carousel
                     firstItem={activeSlide}
                     data={data}
                     // showsHorizontalScrollIndicator={true}
                     renderItem={this.renderItem}
                     sliderWidth={width}
                     sliderHeight={height}
                     itemWidth={width * (60 / 100)}
                     // inactiveSlideOpacity={1}
                     inactiveSlideScale={0.7}
                     onSnapToItem={(index, item) => {
                        let activeImage = data[index].image;
                        let activeDesc = data[index].description;
                        let activeTitle = data[index].title;
                        this.setState({
                           activeSlide: index,
                           activeImage,
                           activeDesc,
                           activeTitle,
                        });
                        if(index == data.length-2 || index == data.length-1){
                           this.onScrollHandler()
                        }
                     }}
                  />
                  {
                     refreshing &&
                     <View style={styles.actInd}>
                        <ActivityIndicator 
                           size='small'
                           color={Colors.primaryGreen4}
                        />
                     </View>
                  }
               </View>
               <View style={[styles.descView, Shadow.shadow]}>
                  <Text style={{color: Colors.primaryGreen4, fontSize: normalize(20), textAlign: 'center', marginBottom: 15 }}>{activeTitle}</Text>
                  <Text style={{color: Colors.primaryGreen4, fontSize: normalize(15), }}>{activeDesc}</Text>
               </View>
            </View>
         </View>
      );
   }

   renderItem = ({item, index}) => {
      return (
         <TouchableOpacity onPress={() => Actions.ArticleDetail({dataItem: item})}>
            <ImageBackground
               defaultSource={require('../assets/images/antibody_img.jpg')}
               source={{ uri: item.image }}
               style={{
                  alignSelf: "center",
                  width: "100%",
                  height: "100%",
                  overflow: "hidden",
                  borderRadius: 10
               }}
            >
               <View style={{width: '100%', height: '40%', backgroundColor: Colors.blackTransparent, bottom: 0, position: 'absolute', padding: 20}}>
                  <Text numberOfLines={1} style={{color: Colors.white, fontSize: normalize(15)}}>{item.title}</Text>
                  <Text numberOfLines={2} style={{color: Colors.white, fontSize: normalize(12), marginTop: 10,}}>{item.description}</Text>
               </View>
            </ImageBackground>
        </TouchableOpacity>
      );
   }

   fetchRecords = (page) => {
      const {userData} = this.props.main
      this.props.dispatch(mainAction.getArticle(page, userData.token));
   }
   
   onScrollHandler = async () => {
      await this.setState({refreshing: true})
      this.setState({
         page: this.state.page + 1
      }, () => this.fetchRecords(this.state.page));
   }

   scrollEnd = ({ layoutMeasurement, contentOffset, contentSize }) => {
      console.log('onotrak scroll', layoutMeasurement, contentOffset, contentSize)
      return layoutMeasurement.width + contentOffset.x >=
          contentSize.width - 20
   };

   handleBackButton = () => {
      // this.props.dispatch({type: RESET_DATA_ARTICLE})
      Actions.pop()
      return true;
   }

};

const mapStateToProps = ({mainReducer}) => ({
  main: mainReducer,
});

export default connect(mapStateToProps)(Article);

const styles = StyleSheet.create({
   container: {
   },
   content: {
      height: height-70,
      // justifyContent: 'center',
      // alignItems: 'center',
      paddingTop: 30,
      backgroundColor: Colors.backgroundColor
   },
   header: {
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: Colors.primaryGreen3,
      width: '100%',
      height: 50,
   },
   textHeader: {
      width: '85%',
      color: Colors.white,
      textAlignVertical: 'center',
      fontSize: normalize(17),
      paddingVertical: 15
   },
   btnHeader: {
      width: '15%',
      paddingHorizontal: 15,
   },
   cardList: {
      padding: 20,
      backgroundColor: Colors.white,
      borderRadius: 3,
      marginVertical: 10,
   },
   cardList1: {
      flexDirection: 'row',
      width: '100%',
   },
   imgList: {
      width: '40%',
      height: 100,
      alignSelf: 'flex-start',
   },
   textView: {
      width: '60%',
      paddingTop: 20,
   },
   titleList: {
      fontSize: normalize(15),
      color: Colors.primaryGreen4,
      textAlign: 'center',
      marginLeft: 10,
   },
   descList: {
      fontSize: normalize(13),
      marginTop: 5,
      color: Colors.primaryGreen4,
   },
   actInd: {
      height: '100%',
      justifyContent: 'center',
      paddingHorizontal: 10,
   },
   descView: {
      flex: 0.5, 
      borderRadius: 10, 
      padding: 20, 
      borderWidth: 1, 
      backgroundColor: Colors.backgroundColor,
      borderColor: Colors.primaryGreen1, 
      margin: 20
   },
});
